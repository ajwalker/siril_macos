# Siril for macOS

![siril](res/siril.png)

The code from this repository produces the official macOS release for [Siril](https://siril.org). Due to its development history, it exists in two places:

| [dehesselle/siril_macos](https://gitlab.com/dehesselle/siril_macos) | [free-astro/siril_macos](https://gitlab.com/free-astro/siril_macos) |
| --- | --- |
| testing purposes; initial prototype | production; build environment for Siril's CI |
| | ![GitLab master branch](https://gitlab.com/free-astro/siril_macos/badges/master/pipeline.svg) ![Latest Release](https://img.shields.io/gitlab/v/release/free-astro/siril_macos?sort=semver&color=2f699b&label=Latest%20Release) |

Development began on the left side and it's still the place where test builds are created on occasion. The right side creates the artifacts for production usage, i.e. what Siril's CI uses to produce the apps.

## download

Siril releases can be downloaded on the official [website](https://siril.org/download/). The app is standalone, relocatable and supports macOS High Sierra up to macOS Ventura.

Development snapshots are available directly from Siril's CI, look for `siril-macos` jobs in the `packaging` stage of a [pipeline](https://gitlab.com/free-astro/siril/-/pipelines) you are interested in.

## data locations

Siril uses the following locations to store data on macOS:

| type | path |
| --- | --- |
| configuration files | `~/Library/Application Support/org.free-astro.Siril` |
| scripts | `~/Library/Application Support/org.free-astro.Siril/scripts` <br> `~/.siril/scripts` <br>  `~/siril/scripts`|
| cache files | `~/Library/Caches/org.free-astro.Siril` |

## license

This work is licensed under [GPL-2.0-or-later](LICENSE).  
Siril is licensed under [GPL-3.0-or-later](https://gitlab.com/free-astro/siril/-/blob/master/LICENSE.md).

### additional credits

Built using other people's work:

- [gtk-osx](https://gitlab.gnome.org/GNOME/gtk-osx) licensed under GPL-2.0-or-later.
