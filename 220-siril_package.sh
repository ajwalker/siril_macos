#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Create the application bundle. This also includes patching library link
# paths and all other components that we need to make relocatable.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

#------------------------------------------------------ source jhb configuration

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

#------------------------------------------- source common functions from bash_d

# bash_d is already available (it's part of jhb configuration)

bash_d_include error
bash_d_include lib

#---------------------------------------------------- source additional settings

source "$(dirname "${BASH_SOURCE[0]}")"/src/siril.sh

### variables ##################################################################

SELF_DIR=$(dirname "${BASH_SOURCE[0]}")

### functions ##################################################################

# Nothing here.

### main #######################################################################

error_trace_enable

#----------------------------------------------------- create application bundle

(
  cd "$SELF_DIR" || exit 1
  export ARTIFACT_DIR   # is referenced in siril.bundle

  jhb run gtk-mac-bundler src/siril.bundle
)

lib_change_paths \
  @executable_path/../Resources/lib \
  "$SIRIL_APP_LIB_DIR" \
  "$SIRIL_APP_CON_DIR"/MacOS/Siril \
  "$SIRIL_APP_CON_DIR"/MacOS/siril-cli

#-------------------------------------------------- add fontconfig configuration

(
  # Recreate all symlinks with relative paths.
  cd "$SIRIL_APP_ETC_DIR"/fonts/conf.d || exit 1
  for file in ./*.conf; do
    ln -sf ../../../share/fontconfig/conf.avail/"$(basename "$file")" .
  done
)

# Our customized version loses all the non-macOS paths and sets a cache
# directory below "Application Support/Caches".
cp "$SELF_DIR"/res/fonts.conf "$SIRIL_APP_ETC_DIR"/fonts


#---------------------------------------------------- add SSL certificate bundle

mkdir "$SIRIL_APP_ETC_DIR"/ca-certificates
cp "$LIB_DIR/python$JHBUILD_PYTHON_VER/site-packages/certifi/cacert.pem" \
  "$SIRIL_APP_ETC_DIR"/ca-certificates

#------------------------------------------------------------- update Info.plist

# enable HiDPI
/usr/libexec/PlistBuddy -c "Add NSHighResolutionCapable bool 'true'" \
  "$SIRIL_APP_PLIST"

# enable dark mode (menubar only, GTK theme is reponsible for the rest)
/usr/libexec/PlistBuddy -c "Add NSRequiresAquaSystemAppearance bool 'false'" \
  "$SIRIL_APP_PLIST"

# set minimum OS version according to deployment target
if [ -z "$MACOSX_DEPLOYMENT_TARGET" ]; then
  MACOSX_DEPLOYMENT_TARGET=$SYS_SDK_VER
fi
/usr/libexec/PlistBuddy \
  -c "Set LSMinimumSystemVersion $MACOSX_DEPLOYMENT_TARGET" \
  "$SIRIL_APP_PLIST"

# set Siril version
/usr/libexec/PlistBuddy -c \
  "Set CFBundleShortVersionString '$(siril_get_version_from_config_h)'" \
  "$SIRIL_APP_PLIST"
/usr/libexec/PlistBuddy -c "Set CFBundleVersion '$SIRIL_BUILD'" \
  "$SIRIL_APP_PLIST"

# set app category
/usr/libexec/PlistBuddy -c \
  "Add LSApplicationCategoryType string 'public.app-category.photography'" \
  "$SIRIL_APP_PLIST"

# set folder access descriptions
/usr/libexec/PlistBuddy -c "Add NSDesktopFolderUsageDescription string \
'Siril needs your permission to access the Desktop folder.'" \
  "$SIRIL_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSDocumentsFolderUsageDescription string \
'Siril needs your permission to access the Documents folder.'" \
  "$SIRIL_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSDownloadsFolderUsageDescription string \
'Siril needs your permission to access the Downloads folder.'" \
  "$SIRIL_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSRemoveableVolumesUsageDescription string \
'Siril needs your permission to access removeable volumes.'" \
  "$SIRIL_APP_PLIST"

# add supported languages
/usr/libexec/PlistBuddy -c "Add CFBundleLocalizations array" \
  "$SIRIL_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add CFBundleLocalizations: string 'en'" \
  "$SIRIL_APP_PLIST"  # because there is no en.po file
for locale in "$SRC_DIR"/siril*/po/*.po; do
  /usr/libexec/PlistBuddy -c "Add CFBundleLocalizations: string \
'$(basename -s .po "$locale")'" "$SIRIL_APP_PLIST"
done

# add some metadata to make CI identifiable
if $CI; then
  for var in PROJECT_NAME PROJECT_URL COMMIT_BRANCH COMMIT_SHA COMMIT_SHORT_SHA\
             JOB_ID JOB_URL JOB_NAME PIPELINE_ID PIPELINE_URL; do
    # use awk to create camel case strings (e.g. PROJECT_NAME to ProjectName)
    /usr/libexec/PlistBuddy -c "Add CI$(\
      echo $var | awk -F _ '{
        for (i=1; i<=NF; i++)
        printf "%s", toupper(substr($i,1,1)) tolower(substr($i,2))
      }'
    ) string $(eval echo \$CI_$var)" "$SIRIL_APP_PLIST"
  done
fi
