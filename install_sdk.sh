#!/usr/bin/env bash

if [ -f "${SDKROOT}/SDKSettings.plist" ]; then
  echo "SDK already installed"
  exit 0
fi

export XCODES_DIRECTORY=$(pwd)/.${SDK_VERSION}
mkdir -p "${XCODES_DIRECTORY}"

export XCODES_USERNAME="${XCODES_USERNAME}"
export XCODES_PASSWORD="${XCODES_PASSWORD}"

echo "Username: ${XCODES_USERNAME}"

# download
xcodes download "${SDK_VERSION}"

# extract
xip -x "${XCODES_DIRECTORY}"/*.xip

# move to sdkroot
cp -r Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk "${SDKROOT}"
rm -rf Xcode.app
